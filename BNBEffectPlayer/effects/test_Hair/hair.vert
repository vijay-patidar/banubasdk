#version 300 es

layout( location = 0 ) in vec3 attrib_pos;

layout(std140) uniform glfx_GLOBAL
{
    mat4 glfx_MVP;
    mat4 glfx_PROJ;
    mat4 glfx_MV;
    vec4 glfx_QUAT;
    vec4 js_hair_color;
};

out vec2 var_uv;
out vec2 var_hairmask_uv;
out vec4 hair_color;

void main()
{
	vec2 v = attrib_pos.xy;
	gl_Position = vec4( v, 1., 1. );
	var_uv = v*0.5 + 0.5;
	var_hairmask_uv = v*vec2(0.5,-0.5) + 0.5;
	hair_color = js_hair_color;
}