//
//  ViewController.swift
//  BanubaCoreSandbox
//
//  Created by Victor Privalov on 7/18/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

import UIKit
import AVKit

import BanubaCore

fileprivate let HAIR_COLOR_RED = 0.0
fileprivate let HAIR_COLOR_GREEN = 0.0
fileprivate let HAIR_COLOR_BLUE = 1.0
fileprivate let HAIR_COLOR_OPACITY = 0.8

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var effectPlayer: EffectPlayer! = EffectPlayer()
    var effects: [String] = []
    var glView: EffectPlayerView!
    
    let playerController = AVPlayerViewController()
    
    var previewImage: UIImage?
    
    @IBOutlet weak var glViewContainer: UIView!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var effectsList: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
          // self.glViewContainer.frame.size = self.view.frame.size
        self.setupGLView()
        self.setupPlayer()
        self.effectPlayer.input.startCamera()
        // Do any additional setup after loading the view, typically from a nib.
        let fm = FileManager.default
        let path = Bundle.main.bundlePath + "/effects"
        
        do {
            effects = try fm.contentsOfDirectory(atPath: path).filter {content in
                var isDir: ObjCBool = false
                fm.fileExists(atPath: path + "/" + content, isDirectory: &isDir)
                return isDir.boolValue
            }
        } catch {
            fatalError("\(error)")
        }
        
        effectsList.dataSource = self
        effectsList.delegate = self
        
        effectPlayer.effects.load(effectName: "test_Hair")
        effectsList.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.effectPlayer.startEffectPlayer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.effectPlayer.stopEffectPlayer()
    }
    
    func setupGLView() {
        glView = EffectPlayerView()
        print("glViewContainer frame size:",self.glViewContainer.bounds)
        // The glViewContainer is the main container which bounds to the full screen of the devices in use.
        
        self.glView.frame = self.glViewContainer.bounds    // set the full screen size to the glView for the AR
        self.glViewContainer.addSubview(self.glView)
    }

    func setupPlayer() {
        
        let basePath = Bundle.main.bundlePath  // By default camera size 720 and 1280

        let configuration = EffectPlayerConfinguration(paths: [basePath + "/effects"],
                cameraSize: CGSize(width: 720, height: 1280),
                screenSize: self.glViewContainer.bounds.size,
                outputScreenSize: CGSize.zero)

        self.effectPlayer.setup(configuration: configuration, delegate: self)
        self.effectPlayer.setRenderTarget(layer: self.glView.layer as? CAEAGLLayer)
        self.effectPlayer.setMaxFaces(count: 2)
    }
    
    func fileURL() -> URL {
        let fileManager = FileManager.default
        let fileUrl = fileManager.temporaryDirectory.appendingPathComponent("video.mp4")

        return fileUrl
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPreview" {
            let previewController = segue.destination as! PreviewController
            previewController.image = self.previewImage
            self.previewImage = nil
        }
    }
    
    func presentVideoController(fileURL:URL) {
        let player = AVPlayer(url: fileURL)
        self.playerController.player = player
        self.present(self.playerController, animated: true, completion: nil)
    }
    
    //MARK: - IBActions
    @IBAction func makePhoto(_ sender: Any) {
        self.effectPlayer.output.takeSnapshot { (image) in
            self.previewImage = image
            self.performSegue(withIdentifier: "toPreview", sender: self)
        }
    }
    
    @IBAction func toggleVideo(_ sender: Any) {
        let shouldRecord = !self.effectPlayer.output.isRecording
        let hasSpace = self.effectPlayer.output.hasDiskCapacityForRecording()
        self.videoButton.setBackgroundImage((shouldRecord ? nil: UIImage(named: "shuter_foto")), for: .normal)
        if shouldRecord && hasSpace {
            let fileURL = self.fileURL()
            self.effectPlayer.input.startAudioCapturing()
            self.effectPlayer.output.startVideoCapturing(fileURL:fileURL) { (success, error) in
                print("Done Writing: \(success)")
                if let _error = error {
                    print(_error)
                }
                self.effectPlayer.input.stopAudioCapturing()
                if success {
                    self.presentVideoController(fileURL: fileURL)
                }
            }
        } else {
            self.effectPlayer.output.stopVideoCapturing(cancel: false)
        }
    }
    @IBAction func clearAction(_ sender: Any) {
        if self.effectPlayer != nil {
            self.effectPlayer = nil
        } else {
            effectPlayer = EffectPlayer()
            self.setupGLView()
            self.setupPlayer()
            self.effectPlayer.input.startCamera()
            self.effectPlayer.startEffectPlayer()
        }
        
    }
    // MARK: Collection  View
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return effects.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "effectCell",
                                                      for: indexPath) as! EffectCell
        let imgPath = Bundle.main.bundlePath + "/effects/" +  effects[indexPath.row]  + "/preview.png"
        var image = UIImage(contentsOfFile: imgPath)
        if image == nil {
            image = UIImage(named: "eyes_prod")
        }
        cell.image.image = image
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        effectPlayer.effects.load(effectName: effects[indexPath.row])
        
    }
    
}

class EffectCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
}

extension ViewController : EffectPlayerDelegate {
    func onBrightnessChanged(isEnough: Bool) {
        
    }
    
    func onCameraPointOfInterest(point: CGPoint) {
        //        print("onCameraPointOfInterest")
    }
    
    func onHintShow(hint: String) {
//        print("onHintShow")
    }
    
    func onHintHide() {
//        print("onHintHide")
    }
    
    func onFacesNumChanged(facesNumber: UInt) {
//        print("onFacesNumChanged")
    }
    
    func onDurationRecognizerCall(instant: Float, averaged: Float) {
//        print("onDurationRecognizerCall")
    }
    
    func onDurationCameraCall(instant: Float, averaged: Float) {
//        print("onDurationCameraCall")
    }
    
    func onDurationRenderCall(instant: Float, averaged: Float) {
//        print("onDurationRenderCall")
        effectPlayer.effects.callJS(method: "setColor", value: "[\(HAIR_COLOR_RED), \(HAIR_COLOR_GREEN), \(HAIR_COLOR_BLUE), \(HAIR_COLOR_OPACITY)]")
    }
}

