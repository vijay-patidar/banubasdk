//
//  Wrapper.swift
//  Easy Snap
//
//  Created by Victor Privalov on 7/12/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

import BanubaCore
import BNBEffectPlayer

public class SandboxApiAdapter: EffectPlayerAPIAdapter {
    
    
    fileprivate  func initPlayer(imageFormat: UnsafePointer<EpImageFormat>,
                                 paths:[String],
                                 delegate:BNBEffectPlayerDelegate) {
        BNBEffectPlayerInit(imageFormat, paths, delegate)
    }
    
    public func initPlayer(configuration: EffectPlayerConfinguration,
                           paths:[String],
                           delegate:EffectPlayer) {
        var imageFormat = EpImageFormat(configuration: configuration)
        self.initPlayer(imageFormat: &imageFormat, paths: paths, delegate: delegate)
    }
    
    public func setConsistency(mode: EffectPlayer.ConsistencyMode) {
        BNBEffectPlayerSetSyncConsistencyMode(mode.rawValue)
    }
    
    // STOP MERGE: must return Int64
    public func draw() -> Int {
        return BNBEffectPlayerDraw()
    }
    public func start() {
        BNBEffectPlayerStart()
    }
    public func stop() {
        BNBEffectPlayerStop()
    }
    public func pause() {
        BNBEffectPlayerPause()
    }
    public func finish() {
        BNBEffectPlayerFinish()
    }
    public func destroy() {
        BNBEffectPlayerDestroy()
    }
    public func surfaceCreated(width: UInt, height: UInt) {
        BNBEffectPlayerSurfaceCreated(width, height)
    }
    public func surfaceDestroyed() {
        BNBEffectPlayerSurfaceDestroyed()
    }
    public func pushFrame(buffer:CVPixelBuffer) {
        BNBEffectPlayerPushFrame(buffer);
    }
    public func setFaceOrientation(angle: Int) {
        BNBEffectPlayerSetFaceOrientation(angle)
    }
    public func loadEffect(name:String) {
        BNBEffectPlayerLoadEffect(name)
    }
    public func unloadEffect() {
        BNBEffectPlayerUnloadEffect()
    }
    public func isEffectLoaded() -> Bool {
        return BNBEffectPlayerIsEffectLoaded()
    }
    public func getMajorVestion() -> UInt {
        return BNBEffectPlayerMajorVersion()
    }
    public func getMinorVestion() -> UInt {
        return BNBEffectPlayerMinorVersion()
    }
    public func callJS(method:String, value:String) {
        BNBEffectPlayerCallJSMethod(method, value)
    }
    public func setMaxFaces(count: UInt) {
        BNBEffectPlayerSetMaxFaces(count)
    }
    public func stopFrameDataCapture() {
        BNBEffectPlayerStopFrameDataCapture()
    }
    public func startFrameDataCapture(folder: String, filename: String?) {
        BNBEffectPlayerStartFrameDataCapture(folder, filename)
    }
    public func processRGBAImage(input: Data, output: NSMutableData, width: UInt, height: UInt) -> Bool {
            return BNBEffectPlayerProcessImageRgba(input, output, UInt32(width), UInt32(height), EPOrientation.angles0, false, EPImagePixelFormat.rgba)
    }
}

extension EffectPlayerOrientation {
    var ep_orientation: EPOrientation {
        return EPOrientation(rawValue: self.rawValue)!
    }
}

extension EpImageFormat {
    init(configuration: EffectPlayerConfinguration) {
        self.init(cameraSize: configuration.cameraSize,
                  screenSize: configuration.screenSize,
                  orientation: configuration.orientation.ep_orientation,
                  isMirrored: ObjCBool(configuration.isMirrored),
                  fov: configuration.fov)
    }
}

extension EffectPlayer: BNBEffectPlayerDelegate {
    public func onError(_ domain: String, message: String) {
        print("Effect player error [\(domain)]: \(message)")
    }
    
    
    public func onIsBrightnessEnoughChanged(_ isEnough: Bool) {
        delegate?.onBrightnessChanged(isEnough: isEnough)
    }
    
    public func onCameraPointOfInterest(_ point: CGPoint) {
        input.setExposurePointOfInterest(point: point)
        delegate?.onCameraPointOfInterest(point: point)
    }
    
    public func onHintShow(_ hint: String) {
        delegate?.onHintShow(hint: hint)
    }
    
    public func onHintHide() {
        delegate?.onHintHide()
    }
    
    public func onFacesNumChanged(_ facesNumber: UInt) {
        delegate?.onFacesNumChanged(facesNumber: facesNumber)
    }
    
    public func onDurationRecognizerCall(_ instant: CGFloat, averaged: CGFloat) {
        delegate?.onDurationRecognizerCall(instant: Float(instant), averaged: Float(averaged))
    }
    
    public func onDurationCameraCall(_ instant: CGFloat, averaged: CGFloat) {
        delegate?.onDurationCameraCall(instant: Float(instant), averaged: Float(averaged))
    }
    
    public func onDurationRenderCall(_ instant: CGFloat, averaged: CGFloat) {
        delegate?.onDurationRenderCall(instant: Float(instant), averaged: Float(averaged))
    }
    
}
