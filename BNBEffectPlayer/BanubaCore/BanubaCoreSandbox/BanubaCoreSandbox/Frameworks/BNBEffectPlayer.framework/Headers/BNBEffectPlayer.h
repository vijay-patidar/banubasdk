//
//  effect_player_wrap-ios.h
//  effect_player_wrap-ios
//
//  Created by Ihar Tumashyk on 9/5/18.
//  Copyright © 2018 banuba. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT const unsigned char BNBEffectPlayerVersionString[];
FOUNDATION_EXPORT const double BNBEffectPlayerVersionNumber;


#import <BNBEffectPlayer/effect_player_msg_wrap-ios.h>
#import <BNBEffectPlayer/BNBOffscreenEffectPlayer.h>
