
#import <Foundation/Foundation.h>
#import "effect_player_msg_wrap-ios.h"

/**
 * Все методы должны вызываться из одного и того же потока
 * (в котором был создан объектBNBOffscreenEffectPlayer)
 * Все методы синхронные

 */
@interface BNBOffscreenEffectPlayer : NSObject

/*
 * в directories можно передать пути где можно найти эффект, если путь к эффектам задается относительно
 * effectWidth andHeight размер внутреней области, в которую рисуется эфект
 */
- (instancetype) initWithDirectories:(NSArray<NSString *> * _Nullable) directories
                         effectWidth:(NSUInteger)width
                           andHeight:(NSUInteger)height;

/*
* EpImageFormat::cameraSize - размер входной RGBA картинки
* EpImageFormat::screenSize не используется
* размер выходной картинки равен размеру внутреней области, в которую рисуется эффект
*/
- (NSData* _Nonnull)processImage:(NSData* _Nonnull)inputRgba
             withFormat:(EpImageFormat* _Nonnull)imageFormat;

- (void)loadEffect:(NSString* _Nonnull)effectName;
- (void)unloadEffect;

/*
 *pause/resume управляет только проигрыванием аудио
 */
- (void)pause;
- (void)resume;

@end
