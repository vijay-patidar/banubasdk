//
//  effect_player_msg_wrap.h
//  effect_player_msg_wrap
//
//  Created by martsinkevich on 15.05.2018.
//  Copyright © 2018 banuba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreVideo/CoreVideo.h>

typedef NS_ENUM(NSUInteger, EPOrientation)
{
    EPOrientationAngles0,
    EPOrientationAngles90,
    EPOrientationAngles180,
    EPOrientationAngles270
};

typedef NS_ENUM(NSUInteger, EPImagePixelFormat)
{
    EPImagePixelFormatRgba,
    EPImagePixelFormatBgra,
    EPImagePixelFormatArgb,
    EPImagePixelFormatRgb,
    EPImagePixelFormatBgr
};

typedef NS_ENUM(NSUInteger, EPSyncConsistencyMode)
{
    EPSyncConsistencyModeSynchronous, //synchronous to recognizer output
    EPSyncConsistencyModeDefaultSynchronousWhenTracking, //uses async-inconsistent mode when no faces found
    EPSyncConsistencyModeAsynchronousInconsistent,
    EPSyncConsistencyModeAsynchronousConsistent
};


typedef struct
{
    CGSize cameraSize;
    CGSize screenSize;
    EPOrientation orientation;
    BOOL isMirrored;
    NSUInteger fov;

} EpImageFormat;


@protocol BNBEffectPlayerDelegate

- (void)onCameraPointOfInterest:(CGPoint)point;

- (void)onHintShow:(nonnull NSString*) hint;
- (void)onHintHide;

- (void)onError:(nonnull NSString*) domain message:(nonnull NSString*) message;

/// Callback that will be called only when amount of found faces changes.
- (void)onFacesNumChanged:(NSUInteger) facesNumber;
- (void)onIsBrightnessEnoughChanged:(BOOL) isEnough;

- (void)onDurationRecognizerCall:(CGFloat) instant averaged:(CGFloat) averaged;
- (void)onDurationCameraCall:(CGFloat) instant averaged:(CGFloat) averaged;
- (void)onDurationRenderCall:(CGFloat) instant averaged:(CGFloat) averaged;

@end

extern const NSInteger BNBDrawSkipped;

FOUNDATION_EXPORT NSUInteger BNBEffectPlayerMajorVersion(void);
FOUNDATION_EXPORT NSUInteger BNBEffectPlayerMinorVersion(void);

/**
 * MUST BE CALLED WITH ACTIVE GL CONTEXT IN RENDER THREAD
 */
FOUNDATION_EXPORT void BNBEffectPlayerInit(const EpImageFormat  * _Nonnull  imageFormat, NSArray<NSString *> * _Nullable directories, id<BNBEffectPlayerDelegate> _Nullable delegate);
FOUNDATION_EXPORT void BNBEffectPlayerDestroy(void);
FOUNDATION_EXPORT NSInteger BNBEffectPlayerDraw(void);

FOUNDATION_EXPORT void BNBEffectPlayerSurfaceCreated(NSUInteger width, NSUInteger height);
FOUNDATION_EXPORT void BNBEffectPlayerSurfaceDestroyed(void);


FOUNDATION_EXPORT BOOL BNBEffectPlayerProcessImageRgba(const NSData* _Nonnull input, NSMutableData* _Nonnull output, unsigned int width, unsigned int height, EPOrientation orientation, BOOL isMirrored, EPImagePixelFormat pixelFormat);

/**
 * Can be called from any thread.
 */
FOUNDATION_EXPORT void BNBEffectPlayerSetImageFormat(const EpImageFormat  * _Nonnull  imageFormat);
FOUNDATION_EXPORT void BNBEffectPlayerSetFaceOrientation(NSInteger angle); // Angle should be a multiple of 90deg.
FOUNDATION_EXPORT void BNBEffectPlayerSetMaxFaces(NSUInteger max_faces); // Sets maximum allowed face results, if face tracking feature is present

FOUNDATION_EXPORT void BNBEffectPlayerPushFrame(_Nonnull CVPixelBufferRef pixelBuffer);
FOUNDATION_EXPORT void BNBEffectPlayerPushFrameWithNum(_Nonnull CVPixelBufferRef pixelBuffer, NSUInteger frameNum);

FOUNDATION_EXPORT void BNBEffectPlayerLoadEffect(NSString * _Nullable effectPath);
FOUNDATION_EXPORT void BNBEffectPlayerUnloadEffect(void);

FOUNDATION_EXPORT void BNBEffectPlayerSetSyncConsistencyMode(const NSUInteger mode); //see EP_SYNC_CONSIST_MODE


/**
 * Returns true if there is an active effect and no effects in loading state.
 * Returns false if there is at least one effect in loading state. Even with active effect.
 */
FOUNDATION_EXPORT BOOL BNBEffectPlayerIsEffectLoaded(void);

/**
 * Adds js method call to call queue. Queue is used on draw call.
 * If there is an effect in loading state, all calls will be performed
 * when new effect loading finished.
 */
FOUNDATION_EXPORT void BNBEffectPlayerCallJSMethod(NSString * _Nonnull method, NSString * _Nonnull value);


FOUNDATION_EXPORT void BNBEffectPlayerStart(void);
FOUNDATION_EXPORT void BNBEffectPlayerStop(void);
FOUNDATION_EXPORT void BNBEffectPlayerPause(void);
FOUNDATION_EXPORT void BNBEffectPlayerResume(void);
FOUNDATION_EXPORT void BNBEffectPlayerFinish(void);

/* Frame data serialization */

/*!
 * \brief Requests to start framedata capture process. Format: CBOR.
 * \param folder Output folder.
 * \param filename Output filename. If the value is empty string, filename is generated based on date and time.
 */
FOUNDATION_EXPORT void BNBEffectPlayerStartFrameDataCapture(NSString * _Nonnull folder, NSString * _Nullable filename);
FOUNDATION_EXPORT void BNBEffectPlayerStopFrameDataCapture(void);

