#!/bin/bash

#BanubaCore Build Script


### Vars
BUILD_DESTINATION=$1
EXEC_PATH="$(cd "$(dirname "$0")" && pwd)"
RUN_PATH=$(pwd)
WORKING_DIRECTORY_NAME=BanubaCoreBuild
WORKING_DIRECTORY=$BUILD_DESTINATION/$WORKING_DIRECTORY_NAME
EFFECT_PLAYER_DIRECTORY=$WORKING_DIRECTORY/EffectPlayerFramework

MODULE_MAP_PATH=$EXEC_PATH/../External/module.modulemap
EFFECT_PLAYER_PATH=$EXEC_PATH/../External/EffectPlayerFramework/Framework/*

### PreCheck

if [ -z "$BUILD_DESTINATION" ]; then
	echo " Error: Build destination not specified"
	exit 1
fi

prepare()
{
	rm -rf $WORKING_DIRECTORY
	mkdir $WORKING_DIRECTORY
}

clean() 
{
	rm -rf $WORKING_DIRECTORY
}

copy_effect_player() 
{
	echo " -- Coping EffectPlayer Framework "
	mkdir $EFFECT_PLAYER_DIRECTORY
	cp $MODULE_MAP_PATH $EFFECT_PLAYER_DIRECTORY/
	cp -r $EFFECT_PLAYER_PATH $EFFECT_PLAYER_DIRECTORY/
}

build() 
{
	echo " -- Building BanubaCore "
	cd $EXEC_PATH
	xcodebuild -scheme BanubaCore build SYMROOT="$WORKING_DIRECTORY" DSTROOT="$WORKING_DIRECTORY" > $WORKING_DIRECTORY/build.log
}

archive() 
{
	echo " -- Archiving BanubaCore "
	cd $BUILD_DESTINATION
	zip -r -X BanubaCore.zip $WORKING_DIRECTORY_NAME >> $WORKING_DIRECTORY/build.log
}

##### Main

echo " ---- Running: [$RUN_PATH], Destination: [$BUILD_DESTINATION], EXEC_PATH [$EXEC_PATH] ----"

prepare
build
copy_effect_player
archive
clean
echo " ---- BanubaCore Build Done ----"

