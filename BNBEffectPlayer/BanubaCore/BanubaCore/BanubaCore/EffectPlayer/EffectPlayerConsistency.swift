//
//  EffectPlayerConsistency.swift
//  BanubaCore
//
//  Created by Victor Privalov on 8/22/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

import Foundation

extension EffectPlayer {
    public enum ConsistencyMode: UInt {
        case synchronous = 0
        case synchronousWhenTracking
        case asynchronousInconsistent
        case asynchronousConsistent
    }
}
