#import "SBVideoWriter.h"
#import <UIKit/UIKit.h>


static const CGFloat kTimescale = 1000000000.0;
static const NSUInteger kLowDiskSpaceLimit = 209715200; // 200 Mb as minimal disk space limit
static const NSUInteger kDiskSpaceCheckTimeInterval = 5; // interval in seconds between checks for available disk space
static NSString *const kLowDiskSpaceRecordingErrorMsg = @"Video recording was terminated due to low disk space."; // TODO adjust text later
static NSString *const kUnknownRecordingErrorMsg = @"Video recording was terminated due to internal issues."; // TODO adjust text later

@implementation SBVideoWriter
{
    AVAssetWriter* _asset_writer;
    
    AVAssetWriterInput* _camera_input;
    AVAssetWriterInput* _microphone_input;
    
    AVAssetWriterInputPixelBufferAdaptor* _pixel_buff_adaptor;
    
    CGSize _captureSize;
    
    CMTime _startTime;
    BOOL _isRealtime;
    
    NSDate *_diskSpaceCheckTime;
    BOOL _errorOccurred;
    
    NSUInteger _audio_frames_cnt;
    NSUInteger _video_frames_cnt;
    
    void(^_completionHandler)(BOOL, NSError *);
    dispatch_once_t onceToken;
}

- (instancetype)initWithCaptureSize:(CGSize)size
{
    self = [super init];
    if (self != nil)
    {
        _captureSize = size;
        _errorOccurred = false;
    }
    
    return self;
}

- (void)dealloc
{
    NSLog(@"DEALLOC WIDEO WRITER");
}

- (void)startCapturingScreenWithUrl:(NSURL *)fileUrl completion:(void(^)(BOOL, NSError *))completionHandler {
    _completionHandler = [completionHandler copy];
    [self prepareInputs:fileUrl];
    [self startCapturing];
}

- (void)startCapturingScreen:(void(^)(BOOL, NSError *))completionHandler {
    _completionHandler = [completionHandler copy];
    [self startCapturing];
}

- (void)pushAudioSampleBuffer:(CMSampleBufferRef)buffer
{
    if (_errorOccurred || _microphone_input == nil)
    {
        return;
    }
    
    NSError *error = nil;
    if (![self isRecordingProcessAvailable:&error]) {
        [self terminateCapturingWithError:error];
        return;
    }
    
    if ([_microphone_input isReadyForMoreMediaData]) {
        if (_startTime.value == 0) {
            NSLog(@"Warning: audio buffer skipped!");
            return;
        }
        
        BOOL success = [_microphone_input appendSampleBuffer:buffer];
        if (success)
        {
            _audio_frames_cnt++;
        }
        else
        {
            [self terminateCapturingWithError:[self.class errorWithMessage:kUnknownRecordingErrorMsg]];
        }
    }
    else
    {
        NSLog(@"Error: audio buffer dropped!");
    }
}

- (void)pushVideoSampleBuffer:(CVPixelBufferRef)buffer
{
    if (_errorOccurred) {
        return;
    }

    NSError *error = nil;
    if (![self isRecordingProcessAvailable:&error]) {
        [self terminateCapturingWithError:error];
        return;
    }
    
    if (![_camera_input isReadyForMoreMediaData])
    {
        NSLog(@"Error: video frame dropped!");
    }
    else
    {
        CFTimeInterval currentTime = CACurrentMediaTime();
        CMTime time = CMTimeMake(currentTime * kTimescale, kTimescale);
        dispatch_once(&onceToken, ^{
            self->_startTime = time;
            [self->_asset_writer startSessionAtSourceTime:self->_startTime];
        });
        
        BOOL success = [_pixel_buff_adaptor appendPixelBuffer:buffer withPresentationTime:time];
        CVPixelBufferRelease(buffer);
        if (success)
        {
            _video_frames_cnt++;
        }
        else
        {
            [self terminateCapturingWithError:[self.class errorWithMessage:kUnknownRecordingErrorMsg]];
        }
    }
}

- (void)prepareInputs:(NSURL *)fileUrl {
    NSError *error = nil;
    _asset_writer = [[AVAssetWriter alloc] initWithURL:fileUrl fileType:AVFileTypeQuickTimeMovie error:&error];
    _asset_writer.shouldOptimizeForNetworkUse = NO;
    NSParameterAssert(error == nil);
    NSParameterAssert(_asset_writer);
    
    CFTimeInterval currentTime = CACurrentMediaTime();
    CMTime time = CMTimeMake(currentTime * kTimescale, kTimescale);
    if (!CMTIME_IS_VALID(time) || CMTIME_IS_INDEFINITE(time) || !CMTIME_IS_NUMERIC(time))
    {
        NSParameterAssert(false);
    }
    NSLog(@"INITIALIZE INPUTS");
    [self setupAssetWriterVideoInput];
    [self setupAssetWriterAudioInput];
    
    [self bindVideoInput];
    [self bindAudioInput];
}

- (void)startCapturing {
    _diskSpaceCheckTime = [NSDate date];
    [_asset_writer startWriting];
    
    NSLog(@"Recording started");
}

- (void)stopCapturing
{
    [self terminateCapturingWithError:nil];
}

- (void)terminateCapturingWithError:(NSError *)error
{
    if (error != nil) {
        _errorOccurred = YES;
    }
    NSLog(@"terminateCapturingWithErrorMessage - %@", error.localizedDescription);
    if (_microphone_input != nil)
    {
        [_microphone_input markAsFinished];
        NSLog(@"microphone_input finished");
    }
    
    if (_camera_input != nil)
    {
        [_camera_input markAsFinished];
        NSLog(@"camera_input finished");
    }
    
    // TODO: Move mimimal frames logic to client side
    AVAssetWriterStatus st = _asset_writer.status;
    
    // TODO: ignore when microphone is off
    BOOL isValid = _video_frames_cnt > 0 && (error == nil);
    
    if (st != AVAssetWriterStatusUnknown)
    {
        __weak typeof(self) weakSelf = self;
        [_asset_writer finishWritingWithCompletionHandler:^{
            __strong typeof(self)strongSelf = weakSelf;
            [strongSelf freeInputs];
            [strongSelf callCompletion:isValid errorDescription:error];
        }];
    }
    else
    {
        [self callCompletion:isValid errorDescription:error];
    }
}

- (void)callCompletion:(BOOL)isValid errorDescription:(NSError *)error {
    if (_completionHandler) {
        _completionHandler(isValid, error);
        _completionHandler = nil;
    }
}

- (void)freeInputs {
    _microphone_input = nil;
    _camera_input = nil;
}

- (void)discardCapturing {
    _completionHandler = nil;
    [self stopCapturing];
}

+ (BOOL)isEnoughDiskSpaceForRecording
{
    NSError *error = nil;
    uint64_t currentAvailableDiskSpace = [self bnb_deviceFreeDiskspaceInBytes:&error];
    return (error == nil) ? (currentAvailableDiskSpace > kLowDiskSpaceLimit) : YES;
}

+ (NSUInteger)bnb_deviceFreeDiskspaceInBytes:(NSError **)pError {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:paths.lastObject error:pError];
    
    if (dictionary) {
        NSNumber *freeFileSystemSizeInBytes = dictionary[NSFileSystemFreeSize];
        return freeFileSystemSizeInBytes.unsignedLongLongValue;
    }
    
    return 0;
}

+ (NSError *)errorWithMessage:(NSString *)message {
    return [NSError errorWithDomain:@"com.banuba.videoWriter" code:1 userInfo:@{NSLocalizedDescriptionKey:message}];
}

# pragma mark - private methods

- (BOOL)isRecordingProcessAvailable:(NSError **)error
{
    if (_asset_writer.status == AVAssetWriterStatusFailed) {
        if (error != nil) {
            *error = _asset_writer.error;
        }
        return NO;
    }
    
    NSDate *currentDate = [NSDate date];
    NSTimeInterval intervalSinceLastCheck = [currentDate timeIntervalSinceDate:_diskSpaceCheckTime];
    if (intervalSinceLastCheck > kDiskSpaceCheckTimeInterval) {
        _diskSpaceCheckTime = currentDate;
        if (![SBVideoWriter isEnoughDiskSpaceForRecording]) {
            *error = [NSError errorWithDomain:@"com.banuba.videoWriter" code:1 userInfo:@{NSLocalizedDescriptionKey:kLowDiskSpaceRecordingErrorMsg}];
            return NO;
        }
    }
    
    return YES;
}

- (void)setupAssetWriterAudioInput
{
    AudioChannelLayout acl;
    bzero(&acl, sizeof(acl));
    acl.mChannelLayoutTag = kAudioChannelLayoutTag_Mono;
    
    NSDictionary*  audioOutputSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [ NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
                                          [ NSNumber numberWithInt: 1 ], AVNumberOfChannelsKey,
                                          [ NSNumber numberWithFloat: 44100.0 ], AVSampleRateKey,
                                          [ NSData dataWithBytes: &acl length: sizeof( AudioChannelLayout ) ], AVChannelLayoutKey,
                                          [ NSNumber numberWithInt: 64000 ], AVEncoderBitRateKey, nil];
    
    _microphone_input = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio outputSettings:audioOutputSettings];
    _microphone_input.expectsMediaDataInRealTime = YES;
    
    NSParameterAssert(_microphone_input);
}

- (void)setupAssetWriterVideoInput
{
    NSDictionary *videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                   AVVideoCodecH264, AVVideoCodecKey,
                                   [NSNumber numberWithInt:_captureSize.width], AVVideoWidthKey,
                                   [NSNumber numberWithInt:_captureSize.height], AVVideoHeightKey,
                                   nil];
    
    _camera_input = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:videoSettings];
    _camera_input.expectsMediaDataInRealTime = YES;
    
    NSDictionary *pixelBufferAdaptorSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [NSNumber numberWithInt:kCVPixelFormatType_32BGRA], kCVPixelBufferPixelFormatTypeKey,
                                                           [NSNumber numberWithInt:_captureSize.width], kCVPixelBufferWidthKey,
                                                           [NSNumber numberWithInt:_captureSize.height], kCVPixelBufferHeightKey,
                                                           nil];
    _pixel_buff_adaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:_camera_input sourcePixelBufferAttributes:pixelBufferAdaptorSettings];
    NSParameterAssert(_camera_input);
}

- (void)bindVideoInput {
    NSParameterAssert([_asset_writer canAddInput:_camera_input]);
    [_asset_writer addInput:_camera_input];
}

- (void)bindAudioInput {
    NSParameterAssert([_asset_writer canAddInput:_microphone_input]);
    [_asset_writer addInput:_microphone_input];
}

@end
