#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface SBVideoWriter : NSObject

- (instancetype)initWithCaptureSize:(CGSize)size;
- (void)pushAudioSampleBuffer:(CMSampleBufferRef)buffer;
- (void)pushVideoSampleBuffer:(CVPixelBufferRef)buffer;

- (void)prepareInputs:(NSURL *)fileUrl;
- (void)startCapturingScreenWithUrl:(NSURL *)fileUrl completion:(void(^)(BOOL, NSError *))completionHandler;
- (void)startCapturingScreen:(void(^)(BOOL, NSError *))completionHandler;
- (void)stopCapturing;
- (void)discardCapturing;
+ (BOOL)isEnoughDiskSpaceForRecording;

@end
