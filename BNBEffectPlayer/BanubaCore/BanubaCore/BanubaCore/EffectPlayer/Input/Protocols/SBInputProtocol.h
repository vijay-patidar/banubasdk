//
//  SBInputProtocol.h
//  SandboxSDK
//
//  Created by Novitskiy on 1/22/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

#ifndef SBInputProtocol_h
#define SBInputProtocol_h

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@protocol SBCameraInputConsumer<NSObject>
- (void)pushBuffer:(CVPixelBufferRef)buffer;
@end


@protocol SBCameraInputProtocol

- (void)setDeviceExposure:(CMTime)exposure andIso:(float)iso;
- (void)setExposurePointOfInterest:(CGPoint)pointOfInterest;

- (BOOL)isZoomFactorAdjustable;
- (CGFloat)minZoomFactor;
- (CGFloat)maxZoomFactor;
- (CGFloat)zoomFactor;
- (void)setZoomFactor:(CGFloat)zoomFactor;

@property (nonatomic, weak) id<SBCameraInputConsumer> delegate;

- (void)start;
- (void)stop;

@end


//--------------------------------------------------------------------------------------------------------------------

@protocol SBMicrophoneInputConsumer<NSObject>

- (void)grabBuffer:(CMSampleBufferRef)buffer;

@end


@protocol SBMicrophoneInputProtocol

@property (nonatomic, weak) id<SBMicrophoneInputConsumer> delegate;

- (void)start;
- (void)stop;

@end



#endif /* SBInputProtocol_h */
