//
//  InputAssembly.swift
//  banubaCam
//
// Created by Denis Kapusta on 5/17/18.
// Copyright (c) 2018 Banuba. All rights reserved.
//

import Foundation

class InputAssembly {
    static func buildInputService(withVideoDelegate videoDelegate: SBCameraInputConsumer, audioDelegate: SBMicrophoneInputConsumer) -> BNCInputService {
        let inputService = BNCInputService()
        inputService.videoDelegate = videoDelegate
        inputService.audioDelegate = audioDelegate

        return inputService
    }
}
