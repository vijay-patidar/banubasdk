//
//  BNCInputService.h
//  banubaCam
//
// Created by Denis Kapusta on 5/17/18.
// Copyright © 2018 Banuba. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SBCameraInputConsumer;
@protocol SBMicrophoneInputConsumer;

NS_ASSUME_NONNULL_BEGIN

@interface BNCInputService : NSObject

@property (nonatomic, weak) id<SBCameraInputConsumer> videoDelegate;
@property (nonatomic, weak) id<SBMicrophoneInputConsumer> audioDelegate;

- (void)setExposurePointOfInterest:(CGPoint)pointOfInterest;

- (BOOL)isZoomFactorAdjustable;
- (CGFloat)minZoomFactor;
- (CGFloat)maxZoomFactor;
- (CGFloat)zoomFactor;
- (CGFloat)setZoomFactor:(CGFloat)zoomFactor;

- (void)startWithAudioEnabled:(BOOL)isAudioEnabled;
- (void)stop;

- (void)setupAudio;
- (void)startAudio;
- (void)stopAudio;

- (void)reSetupAndUseFront:(BOOL)useFront;
- (BOOL)isFrontCamera;

@end

NS_ASSUME_NONNULL_END
