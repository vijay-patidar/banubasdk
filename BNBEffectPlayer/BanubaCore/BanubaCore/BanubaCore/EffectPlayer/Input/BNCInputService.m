//
//  BNCInputService.m
//  banubaCam
//
// Created by Denis Kapusta on 5/17/18.
// Copyright © 2018 Banuba. All rights reserved.
//

#import "BNCInputService.h"
#import "SBCameraInput.h"
#import "SBInputProtocol.h"
#import "SBMicrophoneInput.h"

@interface BNCInputService ()
@property (nonatomic, strong) SBCameraInput *cameraInput;
@property (nonatomic, strong) SBMicrophoneInput *micInput;
@property (nonatomic, assign) BOOL useFront;
@property (nonatomic, assign) BOOL isCapturing;
@end

@implementation BNCInputService

#pragma mark - Properties

- (id <SBCameraInputConsumer>)videoDelegate {
    return self.cameraInput.delegate;
}

- (void)setVideoDelegate:(id <SBCameraInputConsumer>)videoDelegate {
    self.cameraInput.delegate = videoDelegate;
}

- (void)setAudioDelegate:(id <SBMicrophoneInputConsumer>)audioDelegate {
    if (_audioDelegate != audioDelegate) {
        _audioDelegate = audioDelegate;
        
        [self updateMicInputDelegate];
    }
}

- (void)updateMicInputDelegate {
    self.micInput.delegate = self.audioDelegate;
}

#pragma mark - Init

- (instancetype)init {
    self = [super init];
    if (self) {
        [self _setupDefaults];
    }

    return self;
}

- (void)_setupDefaults {
    self.cameraInput = [[SBCameraInput alloc] init];
}

#pragma mark - Public

- (void)setExposurePointOfInterest:(CGPoint)pointOfInterest {
    [self.cameraInput setExposurePointOfInterest:pointOfInterest];
}

- (BOOL)isZoomFactorAdjustable {
    return [self.cameraInput isZoomFactorAdjustable];
}

- (CGFloat)minZoomFactor {
    return [self.cameraInput minZoomFactor];
}

- (CGFloat)maxZoomFactor {
    return [self.cameraInput maxZoomFactor];
}

- (CGFloat)zoomFactor {
    return [self.cameraInput zoomFactor];
}

- (CGFloat)setZoomFactor:(CGFloat)zoomFactor {

    if ([self isZoomFactorAdjustable] == true) {
        CGFloat correctedZoomFactor = MAX([self minZoomFactor], MIN(zoomFactor, [self maxZoomFactor]));

        [self.cameraInput setZoomFactor:correctedZoomFactor];

        return correctedZoomFactor;

    } else {

        return [self minZoomFactor];
    }
}

- (void)startWithAudioEnabled:(BOOL)isAudioEnabled {
    self.isCapturing = YES;
    [self.cameraInput start];
    
    if (isAudioEnabled) {
        [self setupAudio];
    }
}

- (void)stop {
    self.isCapturing = NO;
    [self.cameraInput stop];
}

- (void)setupAudio {
    if (!self.micInput) {
        self.micInput = [[SBMicrophoneInput alloc] init];
        self.micInput.delegate = self.audioDelegate;
    }
}

- (void)startAudio {
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        [self.micInput start];
    });
}

- (void)stopAudio {
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        [self.micInput stop];
    });
}

- (void)reSetupAndUseFront:(BOOL)useFront {
    if (self.cameraInput.useFrontCamera == useFront) {
        return;
    }

    self.cameraInput.useFrontCamera = useFront;

    if (self.isCapturing) {
        [self.cameraInput stop];
    }

    [self.cameraInput rotateCamera];

    if (self.isCapturing) {
        [self.cameraInput start];
    }
}

- (BOOL)isFrontCamera {
    return (self.cameraInput.useFrontCamera == YES);
}

@end
