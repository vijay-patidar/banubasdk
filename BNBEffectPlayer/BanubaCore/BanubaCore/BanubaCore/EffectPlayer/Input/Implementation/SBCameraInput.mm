//
//  SBCameraInput.m
//  SandboxSDK
//
//  Created by Novitskiy on 1/22/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

//#import <Foundation/Foundation.h>

#import "SBCameraInput.h"
#import "SBException.h"

#include <vector>

@implementation SBCameraInput
{
    AVCaptureSession *_session;
    AVCaptureDevice *_camera;
    AVCaptureDeviceInput *_videoInput;
}

@synthesize delegate;

- (instancetype)init
{
    self = [super init];
    if (nil != self)
    {
        _useFrontCamera = YES;
        [self setupAVCapture];
    }
    
    return self;
}

- (void)start
{
    [_session startRunning];
}

- (void)stop
{
    [_session stopRunning];
}

- (void)rotateCamera
{
    
    NSError *error;
    AVCaptureDeviceInput *newVideoInput;
    
    NSArray<AVCaptureDeviceType> *deviceTypes = @[AVCaptureDeviceTypeBuiltInWideAngleCamera];
    AVCaptureDeviceDiscoverySession* discoverySession =  [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:deviceTypes mediaType:AVMediaTypeVideo position:self.useFrontCamera ? AVCaptureDevicePositionFront : AVCaptureDevicePositionBack];
    _camera = [discoverySession.devices firstObject];
    newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:_camera error:&error];
    
    if (newVideoInput != nil)
    {
        [_session beginConfiguration];
        
        [_session removeInput:_videoInput];
        if ([_session canAddInput:newVideoInput])
        {
            [_session addInput:newVideoInput];
            _videoInput = newVideoInput;
        }
        else
        {
            [_session addInput:_videoInput];
        }
        
        [_session.outputs enumerateObjectsUsingBlock:^(__kindof AVCaptureOutput * _Nonnull dataOutput, NSUInteger idx, BOOL * _Nonnull stop) {
            AVCaptureConnection *conn = [dataOutput connectionWithMediaType:AVMediaTypeVideo];
            [conn setVideoOrientation:AVCaptureVideoOrientationPortrait];
            [conn setVideoMirrored:self.useFrontCamera];
        }];
        
        [_session commitConfiguration];
    }
}

- (void)setupAVCapture
{
    _session = [[AVCaptureSession alloc] init];
    
    [_session beginConfiguration];
    
    
    [_session setSessionPreset:AVCaptureSessionPreset1280x720];
    
    NSArray<AVCaptureDeviceType> *deviceTypes = @[AVCaptureDeviceTypeBuiltInWideAngleCamera];
    AVCaptureDeviceDiscoverySession* discoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:deviceTypes mediaType:AVMediaTypeVideo position:self.useFrontCamera ? AVCaptureDevicePositionFront : AVCaptureDevicePositionBack];

    _camera = [discoverySession.devices firstObject];
    if(_camera == nil)
        @throw [[SBException alloc] initWithName:@"AVCaptureDeviceException" reason:@"Failed to initialize video device" userInfo:nil];
    
    NSError *error;
    _videoInput = [AVCaptureDeviceInput deviceInputWithDevice:_camera error:&error];
    
    if (error != nil)
    {
        @throw [[SBException alloc] initWithName:@"VideoDeviceException" reason:error.description userInfo:nil];
    }
    
    [_session addInput:_videoInput];
    
    
    AVCaptureVideoDataOutput * dataOutput = [[AVCaptureVideoDataOutput alloc] init];
    [dataOutput setAlwaysDiscardsLateVideoFrames:YES];
    
    [dataOutput setVideoSettings:@{(id)kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)}];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    [dataOutput setSampleBufferDelegate:self queue:queue];
    [_session addOutput:dataOutput];
    
    
    AVCaptureConnection *conn = [dataOutput connectionWithMediaType:AVMediaTypeVideo];
    [conn setVideoOrientation:AVCaptureVideoOrientationPortrait];
    [conn setVideoMirrored:YES];
    
    [_session commitConfiguration];
}

- (void)dealloc
{
    [self stop];
    
    for (AVCaptureInput *input1 in _session.inputs)
    {
        [_session removeInput:input1];
    }
    
    for (AVCaptureOutput *output1 in _session.outputs)
    {
        [_session removeOutput:output1];
    }
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection
{
    CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    [self.delegate pushBuffer:pixelBuffer];
}


- (void)setDeviceExposure:(CMTime)exposure andIso:(float)iso
{
    if ([_camera lockForConfiguration:nil])
    {
        [_camera setExposureModeCustomWithDuration:exposure
                                               ISO:iso
                                 completionHandler:^(CMTime syncTime)
         {
         }];
    }
}

- (void)setExposurePointOfInterest:(CGPoint)pointOfInterest
{
    NSError* error = nil;
    if (!CGPointEqualToPoint(pointOfInterest, _camera.exposurePointOfInterest))
    {
        if ([_camera lockForConfiguration:&error])
        {
            // Remap point of exposure.
            // exposurePointOfInterest property's CGPoint value uses a coordinate system where {0,0}
            // is the top left of the picture area and {1,1} is the bottom right.
            // This coordinate system is always relative to a landscape device orientation
            // with the home button on the right, regardless of the actual device orientation.
            
            CGPoint point = CGPointMake(pointOfInterest.y, 1.0 - pointOfInterest.x);
            
            _camera.exposurePointOfInterest = point;
            _camera.exposureMode = AVCaptureExposureModeContinuousAutoExposure;
            
            [_camera unlockForConfiguration];
        }
    }
}

- (BOOL)isZoomFactorAdjustable
{
    return fabs([self maxZoomFactor] - [self minZoomFactor]) > FLT_EPSILON;
}

- (CGFloat)minZoomFactor
{
    return 1.0;
}

- (CGFloat)maxZoomFactor
{
    return _camera.activeFormat.videoMaxZoomFactor;
}

- (CGFloat)zoomFactor
{
    return _camera.videoZoomFactor;
}

- (void)setZoomFactor:(CGFloat)zoomFactor
{
    NSError* error = nil;

    if ([_camera lockForConfiguration:&error])
    {
        _camera.videoZoomFactor = zoomFactor;
        [_camera unlockForConfiguration];
    }
}

@end
