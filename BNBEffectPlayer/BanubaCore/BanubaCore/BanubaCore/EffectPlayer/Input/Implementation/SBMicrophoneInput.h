//
//  SBMicrophoneInput.h
//  BanubaSDKExample
//
//  Created by Novitskiy on 4/19/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AVFoundation/AVFoundation.h>
#import "SBInputProtocol.h"

@interface SBMicrophoneInput : NSObject<SBMicrophoneInputProtocol, AVCaptureAudioDataOutputSampleBufferDelegate>

@end
