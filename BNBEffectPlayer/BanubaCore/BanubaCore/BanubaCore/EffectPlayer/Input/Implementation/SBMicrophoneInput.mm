//
//  SBMicrophoneInput.m
//  BanubaSDKExample
//
//  Created by Novitskiy on 4/19/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

#import "SBMicrophoneInput.h"
#import "SBException.h"

@implementation SBMicrophoneInput
{
    AVCaptureSession *_session;
    AVCaptureDevice *_microphone;
}

@synthesize delegate;

- (instancetype)init
{
    self = [super init];
    if (nil != self)
    {
        [self setupAVCapture];
    }
    
    return self;
}

- (void)start
{
    [_session startRunning];
}

- (void)stop
{
    [_session stopRunning];
}


- (void)setupAVCapture
{
    //-- Setup Capture Session.
    _session = [[AVCaptureSession alloc] init];
    _session.usesApplicationAudioSession = NO;
    
    [_session beginConfiguration];
    
    //-- Creata a video device and input from that Device.  Add the input to the capture session.
    NSArray<AVCaptureDeviceType> *deviceTypes = @[AVCaptureDeviceTypeBuiltInMicrophone];
    AVCaptureDeviceDiscoverySession* discoverySession =  [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:deviceTypes mediaType:AVMediaTypeAudio position:AVCaptureDevicePositionUnspecified];
    
    _microphone = [discoverySession.devices firstObject];
    
    
    if(_microphone == nil)
        @throw [[SBException alloc] initWithName:@"AVCaptureDeviceException" reason:@"Failed to initialize microphone device" userInfo:nil];
    
    //-- Add the device to the session.
    NSError *error;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:_microphone error:&error];
    
    if (error != nil)
    {
        @throw [[SBException alloc] initWithName:@"MicrophoneDeviceException" reason:error.description userInfo:nil];
    }
    
    [_session addInput:input];
    
    //-- Create the output for the capture session.
    AVCaptureAudioDataOutput * dataOutput = [[AVCaptureAudioDataOutput alloc] init];
    
    dispatch_queue_t queue = dispatch_get_main_queue();
    [dataOutput setSampleBufferDelegate:self queue:queue];
    [_session addOutput:dataOutput];
    [_session commitConfiguration];
}

- (void)captureOutput:(AVCaptureOutput *)output didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    [self.delegate grabBuffer:sampleBuffer];
}
@end
