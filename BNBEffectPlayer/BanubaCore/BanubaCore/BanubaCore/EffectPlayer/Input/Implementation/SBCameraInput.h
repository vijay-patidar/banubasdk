//
//  SBCameraInput.h
//  SandboxSDK
//
//  Created by Novitskiy on 1/22/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

#ifndef SBCameraInput_h
#define SBCameraInput_h

#import "SBInputProtocol.h"

#import <AVFoundation/AVFoundation.h>


@interface SBCameraInput : NSObject<SBCameraInputProtocol, AVCaptureVideoDataOutputSampleBufferDelegate>

@property (nonatomic, assign) BOOL useFrontCamera;

- (void)setupAVCapture;
- (void)rotateCamera;

@end

#endif /* SBCameraInput_h */
