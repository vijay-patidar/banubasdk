//
//  EffectPlayerDelegate.swift
//  BanubaCore
//
//  Created by Victor Privalov on 7/19/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

public protocol EffectPlayerDelegate: AnyObject {
    
    func onBrightnessChanged(isEnough: Bool)
    
    func onCameraPointOfInterest(point: CGPoint)
    
    func onHintShow(hint: String)
    
    func onHintHide()
    
    func onFacesNumChanged(facesNumber: UInt)
    
    func onDurationRecognizerCall(instant: Float, averaged: Float)
    
    func onDurationCameraCall(instant: Float, averaged: Float)
    
    func onDurationRenderCall(instant: Float, averaged: Float)
}
