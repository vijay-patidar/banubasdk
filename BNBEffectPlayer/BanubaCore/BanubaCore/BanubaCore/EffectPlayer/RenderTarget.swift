//
//  RenderTarget.swift
//  Easy Snap
//
//  Created by Victor Privalov on 7/16/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

public protocol SnapshotProvider {
    func makeSnapshot() -> UIImage
}

public protocol PixelBufferProvider {
    func makeVideoPixelBuffer() -> CVPixelBuffer
}

extension BNBRenderTarget : SnapshotProvider {
    public func makeSnapshot() -> UIImage {
        return snapshot()
    }
}

extension BNBRenderTarget : PixelBufferProvider {
    public func makeVideoPixelBuffer() -> CVPixelBuffer {
        return createRenderedVideoPixelBuffer().takeUnretainedValue()
    }
}
