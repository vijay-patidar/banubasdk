//
//  Wrapper.swift
//  Easy Snap
//
//  Created by Victor Privalov on 7/12/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

public protocol EffectPlayerAPIAdapter {
    func draw() -> Int
    func start()
    func stop()
    func pause()
    func finish()
    func destroy()
    
    func surfaceCreated(width: UInt, height: UInt)
    func surfaceDestroyed()
    
    func pushFrame(buffer:CVPixelBuffer)
    
    func setFaceOrientation(angle:Int)
    
    func loadEffect(name:String)
    func unloadEffect()
    func isEffectLoaded() -> Bool
    
    func getMajorVestion() -> UInt
    func getMinorVestion() -> UInt
    
    func callJS(method:String, value:String)
    
    func setMaxFaces(count:UInt)
    
    func stopFrameDataCapture()
    func startFrameDataCapture(folder: String, filename: String?)
    func setConsistency(mode: EffectPlayer.ConsistencyMode)
    func processRGBAImage(input: Data, output: NSMutableData, width: UInt, height: UInt) -> Bool
    func initPlayer(configuration: EffectPlayerConfinguration, paths:[String], delegate:EffectPlayer)
}

class EffectPlayerAPIAdapterStub: EffectPlayerAPIAdapter {
    public func draw() -> Int {
        print("Using Stab API Adapter: \(#function)")
        return -1
    }
    public func start() {
        print("Using Stab API Adapter: \(#function)")
    }
    public func stop() {
        print("Using Stab API Adapter: \(#function)")
    }
    public func pause() {
        print("Using Stab API Adapter: \(#function)")
    }
    public func finish() {
        print("Using Stab API Adapter: \(#function)")
    }
    public func destroy() {
        print("Using Stab API Adapter: \(#function)")
    }
    public func surfaceCreated(width: UInt, height: UInt) {
        print("Using Stab API Adapter: \(#function)")
    }
    public func surfaceDestroyed() {
        print("Using Stab API Adapter: \(#function)")
    }
    public func pushFrame(buffer: CVPixelBuffer) {
        print("Using Stab API Adapter: \(#function)")
    }
    public func setFaceOrientation(angle: Int) {
        print("Using Stab API Adapter: \(#function)")
    }
    public func loadEffect(name: String) {
        print("Using Stab API Adapter: \(#function)")
    }
    public func unloadEffect() {
        print("Using Stab API Adapter: \(#function)")
    }
    public func isEffectLoaded() -> Bool {
        print("Using Stab API Adapter: \(#function)")
        return false
    }
    public func getMajorVestion() -> UInt {
        print("Using Stab API Adapter: \(#function)")
        return 0
    }
    public func getMinorVestion() -> UInt {
        print("Using Stab API Adapter: \(#function)")
        return 0
    }
    public func callJS(method: String, value: String) {
        print("Using Stab API Adapter: \(#function)")
    }
    public func setMaxFaces(count: UInt) {
        print("Using Stab API Adapter: \(#function)")
    }
    public func stopFrameDataCapture() {
        print("Using Stab API Adapter: \(#function)")
    }
    public func startFrameDataCapture(folder: String, filename: String?) {
        print("Using Stab API Adapter: \(#function)")
    }
    public func setConsistency(mode: EffectPlayer.ConsistencyMode) {
        print("Using Stab API Adapter: \(#function)")
    }
    public func initPlayer(configuration: EffectPlayerConfinguration, paths: [String], delegate: EffectPlayer) {
        print("Using Stab API Adapter: \(#function)")
    }
    public func processRGBAImage(input: Data, output: NSMutableData, width: UInt, height: UInt) -> Bool {
        print("Using Stab API Adapter: \(#function)")
        return false
    }
    
}
