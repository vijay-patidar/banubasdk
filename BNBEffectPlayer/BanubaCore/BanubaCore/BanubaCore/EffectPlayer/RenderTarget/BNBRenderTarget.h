//
//  BNBLayerHandler.h
//  Easy Snap
//
//  Created by Victor Privalov on 7/12/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

#import <GLKit/GLKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BNBRenderTarget : NSObject

@property (nonatomic, weak) EAGLContext *context;
@property (nonatomic, weak) CAEAGLLayer *layer;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithContext:(EAGLContext *)context layer:(CAEAGLLayer *)layer NS_DESIGNATED_INITIALIZER;
- (CVPixelBufferRef)createRenderedVideoPixelBuffer;
- (UIImage *)snapshot;
- (void)activate;
- (void)presentRenderbuffer;
    
- (CGSize)captureSize;
@end

NS_ASSUME_NONNULL_END
