//
//  BNBLayerHandler.m
//  Easy Snap
//
//  Created by Victor Privalov on 7/12/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

#import "BNBRenderTarget.h"
#import <GLKit/GLKit.h>
#import <Accelerate/Accelerate.h>

static const NSInteger kHandlerBackingWidth = 720;
static const NSInteger kHandlerBackingHeight = 1280;
static const NSInteger kHandlerBytesPerPixel = 4;

@implementation BNBRenderTarget
{
    GLuint _colorRenderBuffer;
    GLuint _framebuffer;
    
    vImageConverterRef _pixelBufferConverter;
}

- (instancetype)initWithContext:(EAGLContext *)context layer:(CAEAGLLayer *)layer {
    self = [super init];
    if (self) {
        _layer = layer;
        _context = context;
        _pixelBufferConverter = [self createPixelConverter];
        [self setup];
    }
    return self;
}

- (void)setup {
    [EAGLContext setCurrentContext:self.context];
    
    glGenFramebuffers(1, &_framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);
    
    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    [self.context renderbufferStorage:GL_RENDERBUFFER fromDrawable:self.layer];
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER, _colorRenderBuffer);
}

- (void)dealloc
{
    glDeleteFramebuffers(1, &_framebuffer);
    glDeleteRenderbuffers(1, &_colorRenderBuffer);
}

static void releasePixels(void *info, const void *data, size_t size) {
    free((void*)data);
}

static void releasePixelBufferData(void *releaseRefCon, const void *baseAddress) {
    free((void *)baseAddress);
}

- (CGSize)captureSize {
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    NSInteger targetWidth = (NSInteger)(screenSize.width / screenSize.height * kHandlerBackingHeight);
    return CGSizeMake(targetWidth, kHandlerBackingHeight);
}

- (UIImage *)snapshot {
    [self activate];
    
    CGSize sz = [self captureSize];
    
    NSInteger myDataLength = sz.width * sz.height * 4;
    
    GLubyte* buffer = (GLubyte *)malloc(myDataLength);
    
    int offset = (kHandlerBackingWidth - sz.width) / 2;
    glReadPixels(offset, 0, sz.width, sz.height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    
    int w = (int)sz.width;
    int h = (int)sz.height;
    
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, buffer, myDataLength, releasePixels);
    
    int bitsPerComponent = 8;
    int bitsPerPixel = 32;
    int bytesPerRow = 4 * w;
    
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    
    CGImageRef imageRef = CGImageCreate(w, h, bitsPerComponent, bitsPerPixel, bytesPerRow, colorSpaceRef, bitmapInfo, provider, NULL, NO, renderingIntent);
    // GL uses coordinate system with (0, 0) in bottom left corner, so we need to flip pixels vertically
    UIImage *myImage = [UIImage imageWithCGImage:imageRef scale:1 orientation:UIImageOrientationDownMirrored];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpaceRef);
    
    
    return myImage;
}

- (CVPixelBufferRef)createRenderedVideoPixelBuffer {
    [self activate];
    
    CGSize sz = [self captureSize];
    NSInteger myDataLength = sz.width * sz.height * kHandlerBytesPerPixel;
    GLubyte* buffer = (GLubyte *)malloc(myDataLength);
    int offset = (kHandlerBackingWidth - sz.width) / 2;
    
    glReadPixels(offset, 0, sz.width, sz.height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    CVPixelBufferRef pixelBuffer = nil;
    
    CVPixelBufferCreateWithBytes(kCFAllocatorDefault, sz.width, sz.height, kCVPixelFormatType_32BGRA, buffer, kHandlerBytesPerPixel * sz.width, releasePixelBufferData, NULL, NULL, &pixelBuffer);
    [self processingPixelBuffer:pixelBuffer];
    
    return pixelBuffer;
}

- (void)processingPixelBuffer:(CVPixelBufferRef)pixelBuffer {
    CVPixelBufferLockBaseAddress(pixelBuffer, 0);
    
    vImage_Buffer vImageBuffer = {
        .width = CVPixelBufferGetWidth(pixelBuffer),
        .height = CVPixelBufferGetHeight(pixelBuffer),
        .rowBytes = CVPixelBufferGetBytesPerRow(pixelBuffer),
        .data = CVPixelBufferGetBaseAddress(pixelBuffer)
    };
    
    vImageConvert_AnyToAny(_pixelBufferConverter, &vImageBuffer, &vImageBuffer, nil, kvImageNoFlags);
    // GL uses coordinate system with (0, 0) in bottom left corner, so we need to flip pixels vertically
    vImageVerticalReflect_ARGB8888(&vImageBuffer, &vImageBuffer, kvImageNoFlags);
    
    CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
}


- (vImageConverterRef)createPixelConverter {
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    vImageCVImageFormatRef destinationCvPixelFormat = vImageCVImageFormat_Create(kCVPixelFormatType_32BGRA, kvImage_ARGBToYpCbCrMatrix_ITU_R_709_2, kCVImageBufferChromaLocation_Center, colorSpace, 0);
    
    vImage_CGImageFormat sourceRGBAImageFormat = {
        .bitsPerComponent = 8,
        .bitsPerPixel = 32,
        .colorSpace = CGColorSpaceCreateDeviceRGB(),
        .bitmapInfo = (CGBitmapInfo)kCGImageAlphaLast,
        .version = 0,
        .decode = nil,
        .renderingIntent = kCGRenderingIntentDefault
    };
    
    const CGFloat bgColor[3] = {0.0, 0.0, 0.0};
    vImageConverterRef converter = vImageConverter_CreateForCGToCVImageFormat(&sourceRGBAImageFormat, destinationCvPixelFormat, bgColor, kvImageNoFlags, nil);
    
    vImageCVImageFormat_Release(destinationCvPixelFormat);
    CGColorSpaceRelease(colorSpace);
    
    return converter;
}

- (void)activate {
    [EAGLContext setCurrentContext:self.context];
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
}

- (void)presentRenderbuffer {
    [self.context presentRenderbuffer:GL_RENDERBUFFER];
}

@end
