//
//  EffectsManager.swift
//  BanubaCore
//
//  Created by Victor Privalov on 8/1/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

public protocol EffectsManaging {
    func load(effectName name:String)
    func callJS(method:String, value:String)
    func unloadCurrentEffect()
    var effectIsLoaded: Bool { get }
}

public class EffectsManager: EffectsManaging {
    public func load(effectName name:String) {
        EffectPlayer.apiAdapter.loadEffect(name: name)
    }
    public func callJS(method:String, value:String) {
        EffectPlayer.apiAdapter.callJS(method: method, value: value)
    }
    public func unloadCurrentEffect() {
        EffectPlayer.apiAdapter.unloadEffect()
    }
    public var effectIsLoaded: Bool {
        return EffectPlayer.apiAdapter.isEffectLoaded()
    }
}
