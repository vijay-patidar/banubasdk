//
//  InputService.swift
//  BanubaCore
//
//  Created by Victor Privalov on 7/19/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

public typealias InputServicing = Servicing & CameraServicing & AudioCapturing & CameraZoomable


public protocol Servicing: AnyObject {
    func setInputService(_ inputService: BNCInputService)
    var delegate: InputServiceDelegate? { get set }
}

public protocol CameraServicing: AnyObject {
    
    func startCamera()
    func stopCamera()
    func changeCamera(useFront: Bool)
    func setExposurePointOfInterest(point:CGPoint)
    var isFrontCamera: Bool { get }
    
}

public protocol AudioCapturing: AnyObject {
    func startAudioCapturing()
    func stopAudioCapturing()
}

public protocol CameraZoomable: AnyObject {
    var isZoomFactorAdjustable: Bool { get }
    var minZoomFactor: Float { get }
    var maxZoomFactor: Float { get }
    var zoomFactor: Float { get }
    func setZoomFactor(_ zoomFactor:Float) -> Float
}

public protocol InputServiceDelegate: AnyObject {
    func push(buffer: CVPixelBuffer)
    func push(buffer: CMSampleBuffer)
}

public class InputService: NSObject {
    
    var inputService:BNCInputService!
    public weak var delegate: InputServiceDelegate?
    
    public override init() {
        super.init()
        self.setInputService(BNCInputService())
    }
}

extension InputService: InputServicing {
    
    public func setInputService(_ inputService: BNCInputService) {
        self.inputService = inputService
        self.inputService.videoDelegate = self
        self.inputService.audioDelegate = self
    }
    
    public func setExposurePointOfInterest(point:CGPoint) {
        inputService.setExposurePointOfInterest(point)
    }
    
    public var isZoomFactorAdjustable: Bool {
        return inputService.isZoomFactorAdjustable()
    }
    
    public var minZoomFactor: Float {
        return Float(inputService.minZoomFactor())
    }
    
    public var maxZoomFactor: Float {
        return Float(inputService.maxZoomFactor())
    }
    
    public var zoomFactor: Float {
        return Float(inputService.zoomFactor())
    }
    
    public func setZoomFactor(_ zoomFactor: Float) -> Float {
        return Float(inputService.setZoomFactor(CGFloat(zoomFactor)))
    }
    
    public func startCamera() {
        inputService.start(withAudioEnabled: false)
    }
    
    public func stopCamera() {
        inputService.stop()
    }
    
    public var isFrontCamera: Bool {
        return inputService.isFrontCamera()
    }
    
    public func changeCamera(useFront: Bool) {
        inputService.reSetupAndUseFront(useFront)
    }
    
    public func startAudioCapturing() {
        inputService.setupAudio()
        inputService.startAudio()
    }
    
    public func stopAudioCapturing() {
        inputService.stopAudio()
    }
}


extension InputService : SBCameraInputConsumer {
    public func push(_ buffer: CVPixelBuffer!) {
        self.delegate?.push(buffer: buffer)
    }
}

extension InputService : SBMicrophoneInputConsumer {
    public func grab(_ buffer: CMSampleBuffer!) {
        self.delegate?.push(buffer: buffer)
    }
}
