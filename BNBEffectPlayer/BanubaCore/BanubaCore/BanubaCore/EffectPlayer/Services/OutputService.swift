//
//  OutputService.swift
//  BanubaCore
//
//  Created by Victor Privalov on 7/20/18.
//  Copyright © 2018 Banuba. All rights reserved.
//


public protocol OutputServicing: AnyObject {
    func takeSnapshot(handler:@escaping (UIImage)->Void)
    func processRGBAImage(input: Data, output: NSMutableData, width: UInt, height: UInt) -> Bool
    func startVideoCapturing(fileURL:URL?, completion:@escaping (Bool, Error?)->Void)
    func stopVideoCapturing(cancel:Bool)
    func reset()
    func hasDiskCapacityForRecording() -> Bool
    func preSetup(fileURL:URL)
    var isRecording: Bool {get}
}


public class OutputService {
    struct Defaults {
        static let PixelBufferWriterLimit = 30
    }
    
    var snapshotHandler : ((UIImage)->Void)?
    
    public var captureSize = CGSize(width: 720, height: 1280)
    public var synchronousVideoCapturing = false
    public private (set) var isRecording = false
    
    private var videoWriter: SBVideoWriter?
    private let queue: OperationQueue
    private let fileManager = FileManager.default
    
    init(queue:OperationQueue) {
        self.queue = queue
    }
    
    func removeFile(fileURL:URL) {
        if fileManager.fileExists(atPath: fileURL.path) {
            try? fileManager.removeItem(at: fileURL)
        }
    }
    
    func startVideoCapturing(completion:@escaping (Bool, Error?)->Void) {
        if isRecording {
            return
        }
        isRecording = true
        self.videoWriter!.startCapturingScreen { [weak self] (success, error) in
            self?.videoWriter = nil
            DispatchQueue.main.async {
                completion(success, error)
            }
        }
    }

}

extension OutputService: OutputServicing {
    public func reset() {
        videoWriter = nil
        isRecording = false
    }
    
    public func takeSnapshot(handler:@escaping (UIImage)->Void) {
        snapshotHandler = { (snapshot) in
            DispatchQueue.main.async {
                handler(snapshot)
            }
        }
    }
    
    public func preSetup(fileURL:URL) {
        self.removeFile(fileURL: fileURL)
        self.videoWriter = SBVideoWriter(capture: self.captureSize)
        self.videoWriter!.prepareInputs(fileURL)
    }
    
    public func startVideoCapturing(fileURL:URL?, completion:@escaping (Bool, Error?)->Void) {
        queue.addOperation { [weak self] in
            guard let `self` = self else { return }
            
            if let url = fileURL, self.videoWriter == nil {
                self.preSetup(fileURL: url)
            }
            self.startVideoCapturing(completion: completion)
        }
    }
    
    public func stopVideoCapturing(cancel:Bool) {
        if !isRecording {
            return
        }
        isRecording = false

        queue.addOperation { [weak self] in
            guard let `self` = self else { return }
            
            if cancel {
                self.videoWriter?.discardCapturing()
                self.reset()
            } else {
                self.videoWriter?.stopCapturing()
            }
        }
    }
    
    public func hasDiskCapacityForRecording() -> Bool {
        return SBVideoWriter.isEnoughDiskSpaceForRecording()
    }
    
    public func processRGBAImage(input: Data, output: NSMutableData, width: UInt, height: UInt) -> Bool {
        return EffectPlayer.apiAdapter.processRGBAImage(input: input, output: output, width:width, height: height)
    }
    
    public func process(buffer: CVPixelBuffer) -> CVPixelBuffer? {
        let type = CVPixelBufferGetPixelFormatType(buffer)

        assert(type == kCVPixelFormatType_32RGBA, "inputRgba is not kCVPixelFormatType_32RGBA")

        let width  = CVPixelBufferGetWidth(buffer)
        let height = CVPixelBufferGetHeight(buffer)
        let byteSize = width * height * 4
        let rowByteSize = width * 4

        CVPixelBufferLockBaseAddress(buffer, CVPixelBufferLockFlags.readOnly);
        defer {
            CVPixelBufferUnlockBaseAddress(buffer, CVPixelBufferLockFlags.readOnly)
        }
        let input = CVPixelBufferGetBaseAddress(buffer)!;
        let inData = Data.init(bytesNoCopy: input, count: byteSize,
                               deallocator: Data.Deallocator.none)

        if let rawOutput = malloc(byteSize) {
            defer {
                free(rawOutput)
            }
            let outData = NSMutableData(bytesNoCopy: rawOutput, length: byteSize)

            let success = EffectPlayer.apiAdapter.processRGBAImage(input: inData,output: outData, width: UInt(width), height:UInt(height))
            guard success else {
                return nil
            }

            var pixelBuffer: CVPixelBuffer?
            CVPixelBufferCreateWithBytes(
                kCFAllocatorDefault,
                width,
                height,
                kCVPixelFormatType_32RGBA,
                outData.mutableBytes,
                rowByteSize,
                { (_: UnsafeMutableRawPointer?, base: UnsafeRawPointer?) in
                    free(UnsafeMutableRawPointer(mutating: base))
            },
                nil,
                nil,
                &pixelBuffer);
            
            return pixelBuffer;
        }
        return nil
    }
    
}

//MARK: - Handlers
extension OutputService {
    
    func handle(snapshotProvider provider:SnapshotProvider) {
        guard let handler = snapshotHandler else {
            return
        }
        snapshotHandler = nil
        handler(provider.makeSnapshot())
    }
    
    func handle(bufferProvider provider:PixelBufferProvider) {
        guard isRecording, let writer = self.videoWriter else {
            return
        }
        
        let buffer = provider.makeVideoPixelBuffer()
        
        if queue.operationCount < Defaults.PixelBufferWriterLimit {
            queue.addOperation {
                writer.pushVideoSampleBuffer(buffer)
            }
        }
    }
    
    func handle(audioBuffer buffer:CMSampleBuffer) {
        guard isRecording, let writer = self.videoWriter else {
            return
        }

        queue.addOperation {
            writer.pushAudioSampleBuffer(buffer)
        }
    }
}

private extension DispatchQueue {
    func execute(_ workItem: DispatchWorkItem, sync: Bool) {
        if sync {
            self.sync(execute: workItem)
        } else {
            self.async(execute: workItem)
        }
    }
}
