//
//  EffectPlayerView.swift
//  Easy Snap
//
//  Created by Victor Privalov on 7/9/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

public class EffectPlayerView: UIView {
    override public class var layerClass : AnyClass {
        return CAEAGLLayer.self
    }
}
