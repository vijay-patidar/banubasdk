//
//  EffectPlayer.swift
//  Easy Snap
//
//  Created by Victor Privalov on 7/9/18.
//  Copyright © 2018 Banuba. All rights reserved.
//


public class EffectPlayer {
    
    public static var apiAdapter:EffectPlayerAPIAdapter = EffectPlayerAPIAdapterStub()
    
    struct Defaults {
        static let NothingToDraw = -1;
    }
    
    var inputService: InputServicing = InputService()
    public var input: InputServicing  {
        get {
            return inputService
        }
        set {
            inputService = newValue
            inputService.delegate = self
        }
    }
    
    let effectsManager = EffectsManager()
    public var effects: EffectsManaging  {
        return effectsManager
    }
    
    private lazy var outputService: OutputService = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        
        return OutputService(queue: queue)
    }()
    public var output: OutputServicing  {
        return outputService
    }
    
    public var version: EffectPlayerVersion {
        let major = EffectPlayer.apiAdapter.getMajorVestion()
        let minor = EffectPlayer.apiAdapter.getMinorVestion()
        return EffectPlayerVersion(major: major, minor: minor);
    }
    
    // .synchronousWhenTracking  - that is Default value in SDK, as for now
    public var consistencyMode = EffectPlayer.ConsistencyMode.synchronousWhenTracking {
        didSet {
            setConsistency(mode: consistencyMode)
        }
    }
    
    //MARK: - Delegate
    public internal(set) weak var delegate: EffectPlayerDelegate?
    
    //MARK: - GL
    private let context = EAGLContext(api: .openGLES3)!
    
    private var renderTarget:BNBRenderTarget?
    public func setRenderTarget(layer:CAEAGLLayer?) {
        if let layer = layer {
            renderTarget = BNBRenderTarget(context: self.context, layer: layer)
            outputService.captureSize = renderTarget!.captureSize()
            renderRunLoop.renderQueue.async { [weak self] in
                self?.surfaceCreated(
                    width: UInt(layer.visibleRect.width),
                    height: UInt(layer.visibleRect.height)
                )
            }
        } else {
            stopRenderLoop()
            renderRunLoop.renderQueue.async { [weak self] in
                self?.surfaceDestroyed()
                self?.renderTarget = nil
            }
        }
    }
    
    //MARK: - Device Orientation
    private let deviceOrientationHandler = OrientationHandler()
    private var deviceOrientation = UIDeviceOrientation.portrait
    
    //MARK: - Render RunLoop
    private var renderRunLoop : DisplayLinkRunLoop!
    private func setupRenderRunLoop() {
        renderRunLoop = DisplayLinkRunLoop(label: "com.banubaCore.renderQueue") { [weak self] in
            guard let `self` = self else { return false }
            return self.drawToContext()
        };
    }
    public var renderQueue : DispatchQueue {
        return renderRunLoop.renderQueue
    }
    
    private func startRenderLoop() {
        renderRunLoop.isStoped = false
        renderRunLoop.start()
    }
    
    private func stopRenderLoop() {
        renderRunLoop.isStoped = true
    }
    
    
    //MARK: - App State Handling
    public var shouldAutoStartOnEnterForeground = false
    private var appStateHandler: AppStateHandler!
    private func setupAppStateHandler() {
        self.appStateHandler.add(name: .UIApplicationDidEnterBackground) { [weak self] (_) in
            self?.stopEffectPlayer()
        }
        self.appStateHandler.add(name: .UIApplicationWillEnterForeground) { [weak self] (_) in
            if self!.shouldAutoStartOnEnterForeground {
                self!.startEffectPlayer()
            }
        }
        self.appStateHandler.add(name: .UIApplicationWillTerminate) { [weak self] (_) in
            self?.input.stopCamera()
            self?.destroyEffectsPlayer()
        }
    }
    
    //MARK: - Effect Player life circle
    public private(set) var isLoaded = false
    public init() {
        self.inputService.delegate = self
        setupRenderRunLoop()
    }
    
    deinit {
        inputService.stopCamera()
        if isLoaded {
            stopEffectPlayer()
            destroyEffectsPlayer()
        }
    }
    
    func setupOutputService(captureSize:CGSize) {
        outputService.captureSize = renderTarget?.captureSize() ?? captureSize
        addSnapshotHandler { [weak self] (provider) in
            self?.outputService.handle(snapshotProvider: provider)
        }
        
        addPixelBufferHandler { [weak self] (provider) in
            self?.outputService.handle(bufferProvider: provider)
        }
    }
    
     public func setup(configuration: EffectPlayerConfinguration, delegate: EffectPlayerDelegate) {
        self.delegate = delegate
        self.appStateHandler = AppStateHandler(notificationCenter: configuration.notificationCenter)
        self.setupAppStateHandler()
        
        self.setupOutputService(captureSize: configuration.outputScreenSize)
        self.shouldAutoStartOnEnterForeground = configuration.shouldAutoStartOnEnterForeground
        renderRunLoop.renderQueue.sync {
            self.initPlayer(configuration: configuration, paths: configuration.allPaths, delegate: self)
            self.isLoaded = true
        }
    }
    
    func destroy() {
        isLoaded = false
        stopOrientationDetection()
        destroyPlayer()
    }

}

//MARK: - Orientation Helper
private extension EffectPlayer {
    
    func startOrientationDetection() {
        deviceOrientationHandler.start()
    }
    
    func stopOrientationDetection() {
        deviceOrientationHandler.stop()
    }
}

extension EffectPlayer: InputServiceDelegate {
    public func push(buffer: CMSampleBuffer) {
        guard isLoaded else { return }
        self.outputService.handle(audioBuffer: buffer)
    }
    
    public func push(buffer: CVPixelBuffer) {
        guard isLoaded else { return }
        self.updateOrientationAndPush(frameBuffer: buffer)
    }
}

//MARK: - Drawing And Capturing
extension EffectPlayer {
    
    func updateOrientationAndPush(frameBuffer buffer:CVPixelBuffer) {
        guard isLoaded else { return }
        // TODO: Check performace here.
        // self.deviceOrientationHelper.deviceOrientation get value Sync from other queue
        //
        let isChanged = deviceOrientation != deviceOrientationHandler.deviceOrientation
        if isChanged, let angle = deviceOrientationHandler.deviceOrientation.effectsPlayerAngle {
            deviceOrientation = deviceOrientationHandler.deviceOrientation
            setFaceOrientation(angle: angle)
        }
        //
        pushFrame(frameBuffer: buffer)
    }
    
    func addSnapshotHandler(handler:@escaping ((SnapshotProvider)->Void)) {
        renderRunLoop.addPostRender { [weak self] in
            handler((self?.renderTarget!)!)
        }
    }
    
    func addPixelBufferHandler(handler:@escaping ((PixelBufferProvider)->Void)) {
        renderRunLoop.addPostRender { [weak self] in
            handler((self?.renderTarget!)!)
        }
    }
    
    private func drawToContext() -> Bool {
        guard let renderTarget = self.renderTarget else {
            return false
        }
        renderTarget.activate()
        let needToPresentBuffer = self.draw()
        if needToPresentBuffer == EffectPlayer.Defaults.NothingToDraw {
            return false
        }
        renderTarget.presentRenderbuffer()
        return true
    }
    
}

//MARK: - Effect Player Management

extension EffectPlayer {
    
    public func startEffectPlayer() {
        if (!self.renderRunLoop.isStoped) {
            return
        }
        startOrientationDetection()
        renderRunLoop.renderQueue.sync {
            self.start()
            self.startRenderLoop()
        }
    }
    
    public func stopEffectPlayer() {
        stopOrientationDetection()
        stopRenderLoop()
        renderRunLoop.renderQueue.sync {
            self.stop()
        }
    }
    
    public func destroyEffectsPlayer() {
        stopRenderLoop()
        setRenderTarget(layer: nil)
        renderRunLoop.renderQueue.sync {
            self.stop()
            self.destroy()
        }
    }
    
}

//MARK: - Wrapper Wrapping Public

extension EffectPlayer {
    public func setMaxFaces(count:UInt) {
        EffectPlayer.apiAdapter.setMaxFaces(count: count)
    }
}

//MARK: - Wrapper Wrapping Private
private extension EffectPlayer {
    
    func initPlayer(configuration: EffectPlayerConfinguration,
                         paths:[String],
                         delegate:EffectPlayer) {
        EAGLContext.setCurrent(context)
        EffectPlayer.apiAdapter.initPlayer(configuration: configuration, paths: paths, delegate: delegate)
    }
    
    func destroyPlayer() {
        EAGLContext.setCurrent(context)
        EffectPlayer.apiAdapter.finish()
        EffectPlayer.apiAdapter.destroy()
    }
    
    func draw() -> Int {
        return EffectPlayer.apiAdapter.draw()
    }
    
    func stop() {
        EffectPlayer.apiAdapter.stop()
    }
    
    func start() {
        EffectPlayer.apiAdapter.start()
    }
    
    func pushFrame(frameBuffer buffer:CVPixelBuffer) {
        EffectPlayer.apiAdapter.pushFrame(buffer: buffer)
    }
    
    func surfaceCreated(width: UInt, height: UInt) {
        EAGLContext.setCurrent(context)
        EffectPlayer.apiAdapter.surfaceCreated(width: width, height: height)
    }
    
    func surfaceDestroyed() {
        EAGLContext.setCurrent(context)
        EffectPlayer.apiAdapter.surfaceDestroyed()
    }
    
    func setFaceOrientation(angle:Int) {
        EffectPlayer.apiAdapter.setFaceOrientation(angle: angle)
    }
    
    func setConsistency(mode: EffectPlayer.ConsistencyMode) {
        EffectPlayer.apiAdapter.setConsistency(mode: mode)
    }
    
    func callJS(method: String, value: String) {
        EffectPlayer.apiAdapter.callJS(method: method, value: value)
    }
    
}

private extension UIDeviceOrientation {
    var effectsPlayerAngle: Int? {
        switch self {
        case .portrait:
            return 0
        case .portraitUpsideDown:
            return 180
        case .landscapeLeft:
            return -90
        case .landscapeRight:
            return 90
        default:
            return nil
        }
    }
}

