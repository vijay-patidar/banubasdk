//
//  EffectPlayerVersion.swift
//  BanubaCore
//
//  Created by Victor Privalov on 9/13/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

import Foundation

public struct EffectPlayerVersion {
    let major:UInt
    let minor:UInt
}
