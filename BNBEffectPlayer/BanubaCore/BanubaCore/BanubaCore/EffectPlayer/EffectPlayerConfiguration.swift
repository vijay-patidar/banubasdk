//
//  EffectPlayerConfinguration.swift
//  Easy Snap
//
//  Created by Victor Privalov on 7/16/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

public enum EffectPlayerOrientation: UInt {
    case degrees_0
    case degrees_90
    case degrees_180
    case degrees_270
}

public struct EffectPlayerConfinguration {
    public let paths: [String]
    public let cameraSize: CGSize
    public let screenSize: CGSize
    public let outputScreenSize: CGSize

    
    public let shouldAutoStartOnEnterForeground: Bool
    public let fov: UInt
    public let isMirrored: Bool
    public let orientation: EffectPlayerOrientation
    public let notificationCenter: NotificationCenter
    
    public var allPaths : [String] {
        return paths
    }
    
    public init(paths: [String],
                cameraSize: CGSize,
                screenSize: CGSize,
                outputScreenSize: CGSize,
                orientation: EffectPlayerOrientation = .degrees_0,
                shouldAutoStartOnEnterForeground: Bool = true,
                isMirrored: Bool = false,
                fov : UInt = 0,
                notificationCenter: NotificationCenter = NotificationCenter.default) {
        self.paths = paths
        self.cameraSize = cameraSize
        self.screenSize = screenSize
        
        self.shouldAutoStartOnEnterForeground = shouldAutoStartOnEnterForeground
        self.orientation = orientation
        self.isMirrored = isMirrored
        self.fov = fov
        self.notificationCenter = notificationCenter
        self.outputScreenSize = outputScreenSize
    }
    
}

