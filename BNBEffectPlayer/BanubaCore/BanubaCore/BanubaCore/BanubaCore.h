//
//  BanubaCore.h
//  BanubaCore
//
//  Created by Victor Privalov on 7/18/18.
//  Copyright © 2018 Banuba. All rights reserved.
//

#import "BNBRenderTarget.h"
#import "BNCInputService.h"
#import "SBCameraInput.h"
#import "SBVideoWriter.h"

//! Project version number for BanubaCore.
FOUNDATION_EXPORT double BanubaCoreVersionNumber;

//! Project version string for BanubaCore.
FOUNDATION_EXPORT const unsigned char BanubaCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BanubaCore/PublicHeader.h>


