var searchData=
[
  ['time_5fstamped_5fevent',['time_stamped_event',['../classbnb_1_1time__stamped__event.html',1,'bnb']]],
  ['time_5fstamped_5fevent_3c_20frx_5frecognition_5fresult_20_3e',['time_stamped_event&lt; frx_recognition_result &gt;',['../classbnb_1_1time__stamped__event.html',1,'bnb']]],
  ['touch',['touch',['../structbnb_1_1touch.html',1,'bnb']]],
  ['touch_5fdelegate',['touch_delegate',['../classbnb_1_1touch__delegate.html',1,'bnb']]],
  ['touch_5fevent',['touch_event',['../classbnb_1_1touch__event.html',1,'bnb']]],
  ['transformation',['transformation',['../classbnb_1_1transformation.html',1,'bnb::transformation'],['../classbnb_1_1transformation.html#aac28ae502956c12fc27d668cf46af47b',1,'bnb::transformation::transformation()'],['../classbnb_1_1transformation.html#a8c6dd12dc1da9a1da2c2c47f2347d211',1,'bnb::transformation::transformation(const mat_t &amp;mat)'],['../classbnb_1_1transformation.html#a5f685c721ee4ae349542d41cc97a655a',1,'bnb::transformation::transformation(const float s_x, const float s_y, const float t_x, const float t_y, const rotate_type r)']]],
  ['types',['Types',['../group__types.html',1,'']]]
];
