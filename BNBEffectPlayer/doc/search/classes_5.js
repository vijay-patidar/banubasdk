var searchData=
[
  ['face_5fdata',['face_data',['../structbnb_1_1face__data.html',1,'bnb::face_data'],['../classbnb_1_1interfaces_1_1face__data.html',1,'bnb::interfaces::face_data']]],
  ['frame_5fdata',['frame_data',['../classbnb_1_1frame__data.html',1,'bnb::frame_data'],['../classbnb_1_1interfaces_1_1frame__data.html',1,'bnb::interfaces::frame_data']]],
  ['frx_5fplane',['frx_plane',['../structbnb_1_1frx__plane.html',1,'bnb']]],
  ['frx_5fplane_3c_20constants_3a_3afrx_5fframe_5fw_2c_20constants_3a_3afrx_5fframe_5fh_2c_201_20_3e',['frx_plane&lt; constants::FRX_FRAME_W, constants::FRX_FRAME_H, 1 &gt;',['../structbnb_1_1frx__plane.html',1,'bnb']]],
  ['frx_5fplane_3c_20constants_3a_3afrx_5fframe_5fw_2c_20constants_3a_3afrx_5fframe_5fh_2c_202_20_3e',['frx_plane&lt; constants::FRX_FRAME_W, constants::FRX_FRAME_H, 2 &gt;',['../structbnb_1_1frx__plane.html',1,'bnb']]],
  ['frx_5frecognition_5fresult',['frx_recognition_result',['../structbnb_1_1frx__recognition__result.html',1,'bnb::frx_recognition_result'],['../classbnb_1_1interfaces_1_1frx__recognition__result.html',1,'bnb::interfaces::frx_recognition_result']]],
  ['frx_5fuv_5fplane',['frx_uv_plane',['../structbnb_1_1frx__uv__plane.html',1,'bnb']]],
  ['frx_5fy_5fplane',['frx_y_plane',['../structbnb_1_1frx__y__plane.html',1,'bnb']]],
  ['full_5fimage_5ft',['full_image_t',['../classbnb_1_1full__image__t.html',1,'bnb']]]
];
