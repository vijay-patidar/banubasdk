var searchData=
[
  ['scoped_5fsingleton',['scoped_singleton',['../classbnb_1_1scoped__singleton.html',1,'bnb']]],
  ['scoped_5fsingleton_3c_20utility_5fmanager_20_3e',['scoped_singleton&lt; utility_manager &gt;',['../classbnb_1_1scoped__singleton.html',1,'bnb']]],
  ['sequential_5feffect_5fplayer',['sequential_effect_player',['../classbnb_1_1sequential__effect__player.html',1,'bnb']]],
  ['set_5fmax_5ffaces',['set_max_faces',['../classbnb_1_1effect__player.html#a8be1566d051d1b473ebe116e4f48c4bc',1,'bnb::effect_player::set_max_faces()'],['../classbnb_1_1sequential__effect__player.html#a80953d19b458fa063befdf54657a2b98',1,'bnb::sequential_effect_player::set_max_faces()']]],
  ['set_5fon_5ffaces_5fnum_5fchanged_5fcallback',['set_on_faces_num_changed_callback',['../classbnb_1_1effect__player.html#a176790987a3a9c945f8baeb90370fe52',1,'bnb::effect_player::set_on_faces_num_changed_callback()'],['../classbnb_1_1sequential__effect__player.html#aed935d831b866cde909c34497fcbcc2d',1,'bnb::sequential_effect_player::set_on_faces_num_changed_callback()']]],
  ['simple_5fevent',['simple_event',['../classbnb_1_1simple__event.html',1,'bnb']]],
  ['singleton',['singleton',['../classbnb_1_1singleton.html',1,'bnb']]],
  ['spin_5flock',['spin_lock',['../classbnb_1_1spin__lock.html',1,'bnb']]],
  ['spin_5fmutex',['spin_mutex',['../classbnb_1_1spin__mutex.html',1,'bnb']]],
  ['start_5fframedata_5fcapture',['start_framedata_capture',['../classbnb_1_1effect__player.html#adefafec8d51b43768ccf3cebb92c1d44',1,'bnb::effect_player']]],
  ['static_5fpool_5fallocator',['static_pool_allocator',['../structbnb_1_1static__pool__allocator.html',1,'bnb']]],
  ['static_5fpool_5fallocator_5ffallback',['static_pool_allocator_fallback',['../classbnb_1_1static__pool__allocator__fallback.html',1,'bnb']]],
  ['subscription_5frequest_5ft',['subscription_request_t',['../structbnb_1_1subscription__request__t.html',1,'bnb']]],
  ['surface_5fcreated',['surface_created',['../classbnb_1_1effect__player.html#a0a5cf3781c67b5b1396caa69c8567044',1,'bnb::effect_player::surface_created()'],['../classbnb_1_1sequential__effect__player.html#aabf067ac803bf523e807fc39291e892c',1,'bnb::sequential_effect_player::surface_created()']]],
  ['synchronous',['synchronous',['../classbnb_1_1effect__player.html#afb7d79bda1a8bd6ae88ad05c368eb8e9a1303c4f41a88e739bc859612321b189f',1,'bnb::effect_player']]]
];
