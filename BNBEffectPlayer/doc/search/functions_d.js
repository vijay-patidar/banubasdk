var searchData=
[
  ['saveframe',['saveFrame',['../classcom_1_1banuba_1_1sdk_1_1internal_1_1gl_1_1_egl_surface_base.html#a58575a1ab51c5b03155e5389e73b66f3',1,'com::banuba::sdk::internal::gl::EglSurfaceBase']]],
  ['set_5fmax_5ffaces',['set_max_faces',['../classbnb_1_1effect__player.html#a8be1566d051d1b473ebe116e4f48c4bc',1,'bnb::effect_player::set_max_faces()'],['../classbnb_1_1sequential__effect__player.html#a80953d19b458fa063befdf54657a2b98',1,'bnb::sequential_effect_player::set_max_faces()']]],
  ['set_5fon_5ffaces_5fnum_5fchanged_5fcallback',['set_on_faces_num_changed_callback',['../classbnb_1_1effect__player.html#a176790987a3a9c945f8baeb90370fe52',1,'bnb::effect_player::set_on_faces_num_changed_callback()'],['../classbnb_1_1sequential__effect__player.html#aed935d831b866cde909c34497fcbcc2d',1,'bnb::sequential_effect_player::set_on_faces_num_changed_callback()']]],
  ['setpresentationtime',['setPresentationTime',['../classcom_1_1banuba_1_1sdk_1_1internal_1_1gl_1_1_egl_core.html#abea9ff78383f1e93be56b5a720bfe875',1,'com.banuba.sdk.internal.gl.EglCore.setPresentationTime()'],['../classcom_1_1banuba_1_1sdk_1_1internal_1_1gl_1_1_egl_surface_base.html#aaa3b4fc580855dab029f0597cada3b7b',1,'com.banuba.sdk.internal.gl.EglSurfaceBase.setPresentationTime()']]],
  ['shutdown',['shutdown',['../classcom_1_1banuba_1_1sdk_1_1internal_1_1renderer_1_1_render_thread.html#ad03ece31c43b90581db12c154b3f99b7',1,'com::banuba::sdk::internal::renderer::RenderThread']]],
  ['start_5fframedata_5fcapture',['start_framedata_capture',['../classbnb_1_1effect__player.html#adefafec8d51b43768ccf3cebb92c1d44',1,'bnb::effect_player']]],
  ['swapbuffers',['swapBuffers',['../classcom_1_1banuba_1_1sdk_1_1internal_1_1gl_1_1_egl_core.html#ac4b3a591524f26672131a2a3892bff57',1,'com.banuba.sdk.internal.gl.EglCore.swapBuffers()'],['../classcom_1_1banuba_1_1sdk_1_1internal_1_1gl_1_1_egl_surface_base.html#ac1806c378645083041b0054a0b7c8145',1,'com.banuba.sdk.internal.gl.EglSurfaceBase.swapBuffers()']]]
];
