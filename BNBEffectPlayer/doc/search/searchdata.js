var indexSectionsWithContent =
{
  0: "abcdefgilmnoprstuxy",
  1: "abcdefgilmnoprstuy",
  2: "cdglpstux",
  3: "m",
  4: "cdr",
  5: "ds",
  6: "etu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "functions",
  3: "typedefs",
  4: "enums",
  5: "enumvalues",
  6: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Functions",
  3: "Typedefs",
  4: "Enumerations",
  5: "Enumerator",
  6: "Modules"
};

