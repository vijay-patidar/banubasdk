var searchData=
[
  ['identified_5fclass',['identified_class',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20colors_5ft_20_3e',['identified_class&lt; event_id_t, colors_t &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20event_20_3e',['identified_class&lt; event_id_t, Event &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20eye_5fposition_20_3e',['identified_class&lt; event_id_t, eye_position &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20eye_5fstate_20_3e',['identified_class&lt; event_id_t, eye_state &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20eyes_5fmask_20_3e',['identified_class&lt; event_id_t, eyes_mask &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20frx_5frecognition_5fresult_20_3e',['identified_class&lt; event_id_t, frx_recognition_result &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20frx_5fuv_5fplane_20_3e',['identified_class&lt; event_id_t, frx_uv_plane &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20frx_5fy_5fplane_20_3e',['identified_class&lt; event_id_t, frx_y_plane &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20full_5fimage_5ft_20_3e',['identified_class&lt; event_id_t, full_image_t &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20gesture_5fevent_20_3e',['identified_class&lt; event_id_t, gesture_event &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20gyroscope_5fevent_20_3e',['identified_class&lt; event_id_t, gyroscope_event &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20image_5fbrightness_5ft_20_3e',['identified_class&lt; event_id_t, image_brightness_t &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20life_5fcycle_5fevent_20_3e',['identified_class&lt; event_id_t, life_cycle_event &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20mask_5foutput_3c_20masknumber_2c_20planet_20_3e_20_3e',['identified_class&lt; event_id_t, mask_output&lt; MaskNumber, PlaneT &gt; &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20occlusion_5fmask_20_3e',['identified_class&lt; event_id_t, occlusion_mask &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20simple_5fevent_3c_20t_2c_20count_20_3e_20_3e',['identified_class&lt; event_id_t, simple_event&lt; T, Count &gt; &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['identified_5fclass_3c_20event_5fid_5ft_2c_20touch_5fevent_20_3e',['identified_class&lt; event_id_t, touch_event &gt;',['../classbnb_1_1identified__class.html',1,'bnb']]],
  ['image_5fbrightness_5ft',['image_brightness_t',['../classbnb_1_1image__brightness__t.html',1,'bnb']]],
  ['image_5fformat',['image_format',['../structbnb_1_1image__format.html',1,'bnb']]],
  ['input_5fmanager',['input_manager',['../classbnb_1_1input__manager.html',1,'bnb']]]
];
