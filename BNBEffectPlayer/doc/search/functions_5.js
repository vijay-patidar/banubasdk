var searchData=
[
  ['set_5fmax_5ffaces',['set_max_faces',['../classbnb_1_1effect__player.html#a8be1566d051d1b473ebe116e4f48c4bc',1,'bnb::effect_player::set_max_faces()'],['../classbnb_1_1sequential__effect__player.html#a80953d19b458fa063befdf54657a2b98',1,'bnb::sequential_effect_player::set_max_faces()']]],
  ['set_5fon_5ffaces_5fnum_5fchanged_5fcallback',['set_on_faces_num_changed_callback',['../classbnb_1_1effect__player.html#a176790987a3a9c945f8baeb90370fe52',1,'bnb::effect_player::set_on_faces_num_changed_callback()'],['../classbnb_1_1sequential__effect__player.html#aed935d831b866cde909c34497fcbcc2d',1,'bnb::sequential_effect_player::set_on_faces_num_changed_callback()']]],
  ['start_5fframedata_5fcapture',['start_framedata_capture',['../classbnb_1_1effect__player.html#adefafec8d51b43768ccf3cebb92c1d44',1,'bnb::effect_player']]],
  ['surface_5fcreated',['surface_created',['../classbnb_1_1effect__player.html#a0a5cf3781c67b5b1396caa69c8567044',1,'bnb::effect_player::surface_created()'],['../classbnb_1_1sequential__effect__player.html#aabf067ac803bf523e807fc39291e892c',1,'bnb::sequential_effect_player::surface_created()']]]
];
