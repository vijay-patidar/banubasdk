var searchData=
[
  ['readfromassets',['readFromAssets',['../classcom_1_1banuba_1_1sdk_1_1internal_1_1utils_1_1_file_utils.html#ad49314a075e51fdb3f46aa729dda2579',1,'com::banuba::sdk::internal::utils::FileUtils']]],
  ['recreate',['recreate',['../classcom_1_1banuba_1_1sdk_1_1internal_1_1gl_1_1_window_surface.html#a7de7a9020312cafbf2f46245a680f5b5',1,'com::banuba::sdk::internal::gl::WindowSurface']]],
  ['release',['release',['../classcom_1_1banuba_1_1sdk_1_1internal_1_1gl_1_1_egl_core.html#a612d4e7085fd4c6de7589654b528912f',1,'com.banuba.sdk.internal.gl.EglCore.release()'],['../classcom_1_1banuba_1_1sdk_1_1internal_1_1gl_1_1_offscreen_surface.html#a0d1795ee2e38f6486de5f0969f9cb809',1,'com.banuba.sdk.internal.gl.OffscreenSurface.release()'],['../classcom_1_1banuba_1_1sdk_1_1internal_1_1gl_1_1_window_surface.html#a5caac5121c46f19d37a6f2024c3ed6f2',1,'com.banuba.sdk.internal.gl.WindowSurface.release()']]],
  ['releasesurface',['releaseSurface',['../classcom_1_1banuba_1_1sdk_1_1internal_1_1gl_1_1_egl_core.html#a288dc01c41576e9d1677422716d78152',1,'com::banuba::sdk::internal::gl::EglCore']]]
];
